package org.ecos.logic.dataaccess;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Arrays;
import java.util.List;

public class ParserSax extends DefaultHandler {
    String qName = "";
    private int innerCounter=0;

    private static final List<String> ELEMENT_NAMES = Arrays.asList("nombre","apellido","titulo");

    public ParserSax() {
    }

    @Override
    public void startDocument() throws SAXException {
        System.out.println("Comienzo a leer");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        this.qName = qName;

        String resultedString = "\t".repeat(this.innerCounter) + "<"+qName+">";
        if (!(ELEMENT_NAMES.contains(qName)))
            resultedString+=System.lineSeparator();

        System.out.print(resultedString);

        this.innerCounter++;

    }


    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        this.qName = qName;

        this.innerCounter--;
        String resultedString = "</" + qName + ">";
        if (!(ELEMENT_NAMES.contains(qName)))
            resultedString = "\t".repeat(this.innerCounter) + resultedString;

        System.out.println(resultedString);
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("...............................................");
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if(ELEMENT_NAMES.contains(qName))
            System.out.print(new String(ch, start, length).trim());
    }


}
