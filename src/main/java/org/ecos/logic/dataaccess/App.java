package org.ecos.logic.dataaccess;

import javax.json.*;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;

public class App
{
    public static void main( String[] args ) throws IOException {
        traverseJson(Objects.requireNonNull(App.class.getResource("/riders.json")).getPath());
    }

    private static void traverseJson(String filePath) throws FileNotFoundException {
        JsonValue value = readFile(filePath);
        traverseJson(value,"");
    }

    private static void traverseJson(JsonValue value,String format) {
        if(value.getValueType() == JsonValue.ValueType.OBJECT) {
            Set<String> lerele = value.asJsonObject().keySet();
            for (String key:lerele) {
                System.out.print(format + key + ": " );
                traverseJson(value.asJsonObject().get(key),"\t" + format);
            }
        }
        if(value.getValueType() == JsonValue.ValueType.STRING){
            System.out.println(value);
        }
        if(value.getValueType() == JsonValue.ValueType.NUMBER){
            System.out.println(value);
        }
        if(value.getValueType() == JsonValue.ValueType.ARRAY){
            JsonArray array = value.asJsonArray();
            for (JsonValue item:array.getValuesAs(JsonValue.class)) {
                traverseJson(item,"\t" + format);
            }
        }
    }

    public static JsonValue readFile(String ruta) throws FileNotFoundException {
        try (JsonReader reader = Json.createReader(new FileReader(ruta))) {
            return reader.read();
        }
    }

}
